from asyncio.log import logger
from flask import Flask, jsonify
from pymongo import MongoClient

import uuid

from elasticsearch import Elasticsearch

import random

import logging

logger = logging.getLogger('werkzeug')
logger.setLevel(logging.DEBUG)

app = Flask(__name__)

es = Elasticsearch(["http://elasticsearch:9200/"])

def get_db():
    client = MongoClient(host='mongo',
                         port=27017, 
                         username='root', 
                         password='pass',
                        authSource="admin")
    db = client["animal_db"]
    return db

@app.route('/')
def ping_server():
    return jsonify({"ping": "pong"})

@app.route('/es/health')
def es_health():
    res = es.cluster.health()
    return jsonify(res)

def es_insert_animal():
    animal = inset_animal()
    res = es.index(index="animal_index", body=animal)
    return res

def es_get_animals():
    res = es.search(index="animal_index", body={"query": {"match_all": {}}})
    return res

def inset_animal():
    db = get_db()
    animal = {
        "id": str(uuid.uuid4()),
        "name": "Lion",
        "type": "Mammal",
    }

    db.animal_tb.insert_one(animal)
    animal.pop("_id")
    return animal

def get_animals():
    db = get_db()
    _animals = db.animal_tb.find()
    animals = [{"id": animal["id"], "name": animal["name"], "type": animal["type"]} for animal in _animals]
    return animals

@app.route('/animals')
def get_stored_animals():
    animals = get_animals()
    return jsonify({"animals": animals})

@app.route('/es/add-animal')
def add_animal_to_es():
    res = es_insert_animal()
    return jsonify(res)

@app.route('/es/get-animals')
def get_animals_from_es():
    res = es_get_animals()
    return jsonify(res)

@app.route('/stress-test')
def stress_test():
    random_action = random.randint(0, 4)
    if random_action == 0:
        logger.info("Inserting animal in ES")
        es_insert_animal()
    elif random_action == 1:
        logger.info("Getting animals from ES")
        es_get_animals()
    elif random_action == 2:
        logger.info("Inserting animal in DB")
        get_animals()
    elif random_action == 3:
        logger.info("Getting animals from DB")
        inset_animal()

    return jsonify({"status": "OK"})

if __name__=='__main__':
    app.run(host="0.0.0.0", port=5000)