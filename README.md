# HSA-2 Homework project for Telegraf, InfluxDB and Grafana

This an HSA-2 Homework project to show the TIG (Telegraf, InfluxDB and Grafana) stack.

Nginx before load

![Nginx before load](./screens/nginx-before-load.png?raw=true "Nginx before load")

Nging after load

![Nginx after load](./screens/nginx-after-load.png?raw=true "Nginx after load")

ElasticSearch before load

![ElasticSearch before load](./screens/es-before-load.png?raw=true "ElasticSearch before load")

ElasticSearch after load

![ElasticSearch after load](./screens/es-after-load.png?raw=true "ElasticSearch after load")

MongoDB before load

![MongoDB before load](./screens/mongo-before-load.png?raw=true "MongoDB before load")

MongoDB after load

![MongoDB after load](./screens/mongo-after-load.png?raw=true "MongoDB after load")

Docker before load

![Docker before load](./screens/docker-before-load.png?raw=true "Docker before load")

Docker after load

![Docker after load](./screens/docker-after-load.png?raw=true "Docker after load")

## To reproduce results

Build images

```bash
$ docker-compose build
```

Start stack

```bash
$ docker-compose up
```

Run ab stress test from another terminal

```bash
$ ab -n 1000 -c 100 http://localhost:8080/stress-test
```

## Services and Ports

### Grafana
- URL: http://localhost:3000 
- User: admin 
- Password: admin 

### Telegraf
- Port: 8125 UDP (StatsD input)

### InfluxDB
- Port: 8086 (HTTP API)
- User: admin 
- Password: admin 
- Database: influx


Run the influx client:

```bash
$ docker-compose exec influxdb influx -execute 'SHOW DATABASES'
```

Run the influx interactive console:

```bash
$ docker-compose exec influxdb influx

Connected to http://localhost:8086 version 1.8.0
InfluxDB shell version: 1.8.0
>
```

[Import data from a file with -import](https://docs.influxdata.com/influxdb/v1.8/tools/shell/#import-data-from-a-file-with-import)

```bash
$ docker-compose exec -w /imports influxdb influx -import -path=data.txt -precision=s
```

## License

The MIT License (MIT). Please see [License File](LICENSE) for more information.

