db = db.getSiblingDB("animal_db");
db.animal_tb.drop();

db.animal_tb.insertMany([
    {
        "id": "79f448d1-b4b4-48d0-b679-5f39743525b1",
        "name": "Lion",
        "type": "wild"
    },
    {
        "id": "007567c8-7e84-422b-8e9d-933013a57501",
        "name": "Cow",
        "type": "domestic"
    },
    {
        "id": "b96107b1-1c08-4d59-92e5-ef9ac0a310f0",
        "name": "Tiger",
        "type": "wild"
    },
]);